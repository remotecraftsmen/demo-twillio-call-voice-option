var path = require('path');
var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var twilio = require('twilio');
var VoiceResponse = twilio.twiml.VoiceResponse;
var config = require('../config');

// Create a Twilio REST API client for authenticated requests to Twilio
var client = twilio(config.accountSid, config.authToken);


// Configure application routes
module.exports = function (app) {
    // Set Jade as the default template engine
    app.set('view engine', 'jade');

    // Express static file middleware - serves up JS, CSS, and images from the
    // "public" directory where we started our webapp process
    app.use(express.static(path.join(process.cwd(), 'public')));

    // Parse incoming request bodies as form-encoded
    app.use(bodyParser.urlencoded({
        extended: true,
    }));

    // Use morgan for HTTP request logging
    app.use(morgan('combined'));

    // Home Page with Click to Call
    app.get('/', function (request, response) {
        response.render('index');
    });

    // Handle an AJAX POST request to place an outbound call
    app.post('/call', function (request, response) {
        // This should be the publicly accessible URL for your application
        // Here, we just use the host for the application making the request,
        // but you can hard code it or use something different if need be
        var customerNumber = request.body.customerNumber;
        var customerName = request.body.customerName;
        var url = config.baseUrl + '/outbound/' + encodeURIComponent(customerNumber) + '/' + encodeURIComponent(customerName);

        var options = {
            to: request.body.businessNumber,
            from: config.twilioNumber,
            url: url,
        };

        // Place an outbound call to the user, using the TwiML instructions
        // from the /outbound route
        client.calls.create(options)
            .then((message) => {
                console.log(message.responseText);
                response.send({
                    message: 'Thank you! We will be calling you shortly.',
                });
            })
            .catch((error) => {
                console.error(error);
                response.status(500).send(error);
            });
    });

    // Return TwiML instuctions for the outbound call 
    app.post('/outbound/:customerNumber/:customerName', function (request, response) {
        var customerNumber = request.params.customerNumber;
        var customerName = request.params.customerName;
        var twimlResponse = new VoiceResponse();

        const gather = twimlResponse.gather({
            action: '/gather/' + encodeURIComponent(customerNumber) + '/' + encodeURIComponent(customerName),
            numDigits: '1',
        });

        gather.say(
            'You have a call request from' + customerName +
            'Do you want to connect to the customer? ' +
            'Please press 1 to accept. ' +
            'Press 2 to decline.',
        );

        twimlResponse.redirect('/outbound/' + encodeURIComponent(customerNumber) + '/' + encodeURIComponent(customerName));

        response.send(twimlResponse.toString());
    });


    app.post('/gather/:customerNumber/:customerName', (request, response) => {

        var customerNumber = request.params.customerNumber;
        var customerName = request.params.customerName;
        // Use the Twilio Node.js SDK to build an XML response
        const twiml = new VoiceResponse();

        // If the user entered digits, process their request
        if (request.body.Digits) {
            switch (request.body.Digits) {
                case '1':
                    twiml.say('You accepted the call. Dialing number');
                    twiml.dial(customerNumber);
                    break;
                case '2':
                    twiml.say('Thank you, Goodbye.');
                    twiml.hangup();
                    break;
                default:
                    twiml.say("Sorry, I don't understand that choice.");
                    twiml.redirect('/outbound/' + encodeURIComponent(customerNumber) + '/' + encodeURIComponent(customerName));
                    break;
            }
        } else {
            // If no input was sent, redirect to the /outbound route
            twiml.redirect('/outbound/' + encodeURIComponent(customerNumber) + '/' + encodeURIComponent(customerName));
        }

        response.send(twiml.toString());
    });

};
