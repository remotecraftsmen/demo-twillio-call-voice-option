'use strict';

const twilio = require('twilio');
const config = require('./config.json');
const express = require('express');
const app = express;

const client = twilio(config.accountSid, config.authToken);

const VoiceResponse = twilio.twiml.VoiceResponse;

const projectId = process.env.GCLOUD_PROJECT;
const region = config.region;

// Handle an AJAX POST request to place an outbound call
exports.call = function (request, response) {
    // This should be the publicly accessible URL for your application
    // Here, we just use the host for the application making the request,
    // but you can hard code it or use something different if need be
    var customerNumber = request.body.customerNumber;
    var customerName = request.body.customerName;
    var url = `https://${region}-${projectId}.cloudfunctions.net` + '/outbound?customerNumber=' + encodeURIComponent(customerNumber) + '&customerName=' + encodeURIComponent(customerName);

    var options = {
        to: request.body.businessNumber,
        from: config.twilioNumber,
        url: url,
    };

    // Place an outbound call to the user, using the TwiML instructionscall
    // from the /outbound route
    client.calls.create(options)
        .then((message) => {
            console.log(message.responseText);
            response.send({
                message: 'Thank you! We will be calling you shortly.',
            });
        })
        .catch((error) => {
            console.error(error);
            response.status(500).send(error);
        });
}

// Return TwiML instuctions for the outbound call 
exports.outbound = function (request, response) {

    var customerNumber = request.query.customerNumber;
    var customerName = request.query.customerName;
    var twimlResponse = new VoiceResponse();

    const gather = twimlResponse.gather({
        action: '/gather?customerNumber=' + encodeURIComponent(customerNumber) + '&customerName=' + encodeURIComponent(customerName),
        numDigits: '1',
    });

    gather.say(
        'You have a call request from' + customerName +
        'Do you want to connect to the customer? ' +
        'Please press 1 to accept. ' +
        'Press 2 to decline.',
    );

    twimlResponse.redirect('/outbound?customerNumber=' + encodeURIComponent(customerNumber) + '&customerName=' + encodeURIComponent(customerName));

    response.send(twimlResponse.toString());
}

exports.gather = function (request, response) {

    var customerNumber = request.query.customerNumber;
    var customerName = request.query.customerName;
    // Use the Twilio Node.js SDK to build an XML response
    const twiml = new VoiceResponse();

    // If the user entered digits, process their request
    if (request.body.Digits) {
        switch (request.body.Digits) {
            case '1':
                twiml.say('You accepted the call. Dialing number');
                twiml.dial(customerNumber);
                break;
            case '2':
                twiml.say('Thank you, Goodbye.');
                twiml.hangup();
                break;
            default:
                twiml.say("Sorry, I don't understand that choice.");
                twiml.redirect('/outbound?customerNumber=' + encodeURIComponent(customerNumber) + '&customerName=' + encodeURIComponent(customerName));
                break;
        }
    } else {
        // If no input was sent, redirect to the /outbound route
        twiml.redirect('/outbound?customerNumber=' + encodeURIComponent(customerNumber) + '&customerName=' + encodeURIComponent(customerName));
    }

    response.send(twiml.toString());
}